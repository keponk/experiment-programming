let svgEl;
let winWidth = window.innerWidth - 20;
// let winHeight = window.innerHeight - 100;
let currentTrial = null;
let trialResults=[];
let csv = [];
let csvTitles = ["DesignName","ParticipantID","TrialID","Block1","Trial","VV","OC","Time","Errors"];
csv.push(csvTitles.join(","));

// console.log("window width: " + winWidth + "   height: " + winHeight);
let ctx = {  
  w: winWidth,
  h: 540,

  trials: [],
  participant: "",
  startBlock: 0,
  startTrial: 0,
  cpt: 0,

  participantIndex:"ParticipantID",
  blockIndex:"Block1",
  trialIndex:"Trial",
  vvIndex:"VV",
  objectsCountIndex:"OC",
};

// window.onresize = function() {
//   console.log("resizing");
//   if (currentTrial !== null){
//     console.log(window.innerWidth);
//     currentTrial.reposition(window.innerWidth - 20);
//   }
// };


let keyListener = function(event){
  event.preventDefault();
  if(currentTrial !== null){
    if ( event.keyCode === 32 ){ // 32 = 'Space'
      // console.log("space!");
      if ( currentTrial.getStatusIndex() === 1){
        currentTrial.stopCounter();
        currentTrial.updateStatus();
      }
    }
    if ( event.keyCode === 13 ){ // 13 = 'Enter'
      // console.log("Enter");
      if ( currentTrial.getStatusIndex() === 0){
        currentTrial.updateStatus();
      }
    }
  }
}

let nextTrial = function() {
  let vv = ctx.trials[ctx.cpt]["VV"];
  let oc = ctx.trials[ctx.cpt]["OC"];
  let trialNumber = ctx.trials[ctx.cpt].TrialID;
  let participant = ctx.trials[ctx.cpt].ParticipantID;
  let block = ctx.trials[ctx.cpt].Block1;
  console.log("Participant:" + participant + "\nBlock: " + block + "\nTrial: " + trialNumber)

  if (ctx.trials[ctx.cpt].ParticipantID === ctx.participant && ctx.cpt<ctx.trials.length-1 ) {
    ctx.cpt++;
    currentTrial = new Trial(vv, oc, svgEl, nextTrial, addResults);
  } else {
    let csvContent="data:text/csv:charset=utf-8,";
    csv.forEach(function(rowArray){
      csvContent+=rowArray+"\r\n";
      console.log(rowArray);
    });
    let encodeUri=encodeURI(csvContent);
    let link = document.createElement("a");
    link.setAttribute("href", encodeUri);
    link.setAttribute("download", `ParticipantID_${ctx.participant}_results.csv`);
    document.body.appendChild(link);
    link.click();
  }
}

let addResults = function (data) {
  let row = [
    ctx.trials[ctx.cpt-1]["DesignName"],
    ctx.trials[ctx.cpt-1]["ParticipantID"],
    ctx.trials[ctx.cpt-1]["TrialID"],
    ctx.trials[ctx.cpt-1]["Block1"],
    ctx.trials[ctx.cpt-1]["Trial"],
    ctx.trials[ctx.cpt-1]["VV"],
    ctx.trials[ctx.cpt-1]["OC"],
    data[0],
    data[1]
    ];
  // console.log(row);
  csv.push(row.join(","));
}

let startExperiment = function(event) {
  event.preventDefault();
  // console.log(event);
  event.srcElement.blur();

  for(let i = 0; i < ctx.trials.length; i++) {
    if(ctx.trials[i][ctx.participantIndex] === ctx.participant) {
      if(parseInt(ctx.trials[i][ctx.blockIndex]) == ctx.startBlock) {
        if(parseInt(ctx.trials[i][ctx.trialIndex]) == ctx.startTrial) {
          ctx.cpt = i;
        }
      }
    }
  }
  console.log("start experiment at "+ctx.cpt);
  console.log("trial participant = " + ctx.trials[ctx.cpt].ParticipantID)
  if (currentTrial !== null){
    currentTrial.resetErrors();
  }
  nextTrial();
}

let createScene = function(){
  svgEl = d3.select("#scene").append("svg");
  svgEl.attr("id", "mainScene");
  svgEl.attr("width", ctx.w);
  svgEl.attr("height", ctx.h)
    .classed('centered', true);

  loadData();
};

/****************************************/
/******** STARTING PARAMETERS ***********/
/****************************************/

let setTrial = function(trialID) {
  ctx.startTrial = parseInt(trialID);
}

let setBlock = function(blockID) {
  ctx.startBlock = parseInt(blockID);

  let trial = "";
  let options = [];

  for(let i = 0; i < ctx.trials.length; i++) {
    if(ctx.trials[i][ctx.participantIndex] === ctx.participant) {
      if(parseInt(ctx.trials[i][ctx.blockIndex]) == ctx.startBlock) {
        if(!(ctx.trials[i][ctx.trialIndex] === trial)) {
          trial = ctx.trials[i][ctx.trialIndex];
          options.push(trial);
        }
      }
    }
  }

  let select = d3.select("#trialSel");

  select.selectAll('option')
    .data(options)
    .enter()
    .append('option')
    .text(function (d) { return d; });

  setTrial(options[0]);

}

let setParticipant = function(participantID) {
  ctx.participant = participantID;

  let block = "";
  let options = [];

  for(let i = 0; i < ctx.trials.length; i++) {
    if(ctx.trials[i][ctx.participantIndex] === ctx.participant) {
      if(!(ctx.trials[i][ctx.blockIndex] === block)) {
        block = ctx.trials[i][ctx.blockIndex];
        options.push(block);
      }
    }
  }

  let select = d3.select("#blockSel")
  select.selectAll('option')
    .data(options)
    .enter()
    .append('option')
    .text(function (d) { return d; });

  setBlock(options[0]);

};

let loadData = function(){

  d3.csv("experiment.csv").then(function(data){
    ctx.trials = data;

    let participant = "";
    let options = [];

    for(let i = 0; i < ctx.trials.length; i++) {
      if(!(ctx.trials[i][ctx.participantIndex] === participant)) {
        participant = ctx.trials[i][ctx.participantIndex];
        options.push(participant);
      }
    }

    let select = d3.select("#participantSel")
    select.selectAll('option')
      .data(options)
      .enter()
      .append('option')
      .text(function (d) { return d; });

    setParticipant(options[0]);

  }).catch(function(error){console.log(error)});
};

function onchangeParticipant() {
  selectValue = d3.select('#participantSel').property('value');
  setParticipant(selectValue);
};

function onchangeBlock() {
  selectValue = d3.select('#blockSel').property('value');
  setBlock(selectValue);
};

function onchangeTrial() {
  selectValue = d3.select("#trialSel").property('value');
  setTrial(selectValue);
};
