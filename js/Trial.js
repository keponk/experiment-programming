class Trial{

  constructor(_vv, _oc, _svgContainer, _callback, _results){
    let ocList = {
      small: 9,
      medium: 25,
      large: 49
    };
    this.elementSize = 50;
    this.padding = 10;
    this.results = _results;
    this.callback = _callback;
    this.visualVariable = _vv;
    this.countName = _oc;
    this.objectCount = ocList[_oc];
    this.matrixSize = Math.sqrt(this.objectCount);
    // this.container = _svgContainer;
    this.svgContainer = _svgContainer;
    // this.containerWidth = _svgContainer.style("width").replace("px","");
    // let containerHeight = _svgContainer.style("height").replace("px","");
    // this.transformValue = "translate("+100+","+100+")";
    this.errorCount = 0;
    this.timeCounter;
    this.statusIndex;
    this.init();
  }

  calculateStartPosition(_cx, _cy){
    let ratio = ((this.elementSize+this.padding)*this.matrixSize) /2;
    let startX = _cx - ratio;
    return `translate(${startX},${_cy})`;
  }

  resetErrors(){
    this.errorCount = 0;
  }

  init(){
    this.statusIndex=-1;
    this.targetIndex = Math.floor(Math.random()*this.objectCount);
    // this.targetIndex = 0;
    this.svgContainer.select("#trialSvg").remove();
    this.containerWidth = this.svgContainer.style("width").replace("px","");
    let transformValue = this.calculateStartPosition(this.containerWidth / 2, this.elementSize);
    this.svgContainer.append("g")
    .attr("id", "trialSvg")
    .attr("transform", transformValue);
    this.timeCounter=0;
    console.log("VV: " + this.visualVariable + "; OC: " + this.objectCount);
    console.log("target Index: " + this.targetIndex);
    this.updateStatus();
  }


  updateStatus(){
    this.statusIndex++;
    switch(this.statusIndex){
      case 0: // CLICKED GO
        document.getElementById("welcome").style.display = "block";
        break;
      case 1: // PRESSED ENTER
        document.getElementById("welcome").style.display = "none";
        this.showTrial();
        break;
      case 2: // PRESSED SPACE
        this.showSquares();
        break;
      case 3: // CHOSE WRONG SQUARE
        this.init();
        break;
      case 4: // CHOSE CORRECT SQUARE
        //write to file
        let resultsArray = [];
        resultsArray.push(this.timeCounter);
        resultsArray.push(this.errorCount);
        this.results(resultsArray);
        this.svgContainer.select("#trialSvg").remove();
        this.callback();
        break;
    }
  }

  showSquares(){
    this.svgContainer.select("#localGroup").remove();
    let localGroup = this.svgContainer.select("#trialSvg")
      .append("g")
      .attr("id", "localGroup");
    for (let i = 0; i < this.objectCount; i++) {
      let size = 50;
      let padding =size/2;
      let rect = localGroup.append("rect")
        .attr("x", i%this.matrixSize*60)
        .attr("y", Math.floor(i/this.matrixSize)*60)
        .attr("width", size)
        .attr("height", size)
        .attr("fill", "gray")
        .on("click", () => {
          this.squareClick(false);
        });
      if (i === this.targetIndex) {
        rect.on("click", () => {
          this.squareClick(true);
        });
      }
    }
  }

  getStatusIndex(){
    return this.statusIndex;
  }

  stopCounter(){
    this.timeCounter = Date.now() - this.timeCounter;
    // console.log("time: "+ this.timeCounter + "ms");
  }

  squareClick(_isRight){
    if(_isRight){
      this.statusIndex++;
      this.updateStatus();
    } else {
      this.errorCount++;
      this.updateStatus();
    }
  }

  getCombination(_shape, _targetShape, _color, _targetColor){
    let circlesVars = [];
    for (let i = 0; i < 2; i++) {
      let obj = {shape: _targetShape, color: _color};
      circlesVars.push(obj);
    }
    for (let i = 0; i < 2; i++) {
      let obj = {shape: _shape, color: _targetColor};
      circlesVars.push(obj);
    }
    for (let i = 0; i < 2; i++) {
      let obj = {shape: _shape, color: _color};
      circlesVars.push(obj);
    }
    for (let i = 0; i < this.objectCount - 7; i++) {
      let randShape = Math.random() > 0.5 ? _shape : _targetShape;
      let randColor = randShape === _targetShape ? _color : _targetColor;
      let obj = {shape: randShape, color: randColor};
      circlesVars.push(obj);
    }
    // for (let i = 0; i < circlesVars.length; i++) {
    //   console.log(circlesVars[i]);
    // }
    this.shuffleArray(circlesVars);
    let targetObj = {shape: _targetShape, color: _targetColor};
    circlesVars.splice(this.targetIndex, 0, targetObj);
    // for (let i = 0; i < circlesVars.length; i++) {
    //   console.log(circlesVars[i]);
    // }
    return circlesVars;
  }

  shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }

  showTrial(){
    let localGroup = this.svgContainer.select("#trialSvg")
      .append("g")
      .attr("id", "localGroup");
    let paddingRatio = this.elementSize+this.padding;

    let color = "blue";
    let targetColor = "red";
    if( Math.random() > 0.5) [color, targetColor] = [targetColor, color];
    
    let circleSample = localGroup.append("circle")
      .attr("id", "circleSample")
      .attr("cx", this.elementSize/2).attr("cy", this.elementSize/2)
      .attr("r", this.elementSize/2)
      .attr("fill", color)
      .attr("stroke", "black").attr("stroke-width", 2);

    let points = `${this.elementSize/2},0 ${this.elementSize},${this.elementSize} 0,${this.elementSize}`;
    let polySample = localGroup.append("polygon")
      .attr("id", "polySample")
      .attr("points", points)
      .attr("stroke", "black")
      .attr("stroke-width", 2)
      .attr("fill", color);

    let shape = circleSample;
    let targetShape = polySample;

    if( Math.random() > 0.5) [shape, targetShape] = [targetShape, shape];
    console.log("targetshapeID: " + targetShape.attr("id"));
    
    if ( this.visualVariable === "ShapeColor"){
      let combination = this.getCombination( shape, targetShape, color, targetColor );
      for (let i = 0; i< this.objectCount; i++){
        let posX = i%this.matrixSize*paddingRatio;
        let posY = Math.floor(i / this.matrixSize) * paddingRatio;
        let position = `translate(${posX},${posY})`;
        let newID = `shape${i}`;
        let newNode = combination[i].shape.clone()
          .attr("id", newID)
          .attr("fill", combination[i].color)
          .attr("transform", position);
      }
    } else {
      for (let i = 0; i < this.objectCount; i++) {
        let posX = i%this.matrixSize*paddingRatio;
        let posY = Math.floor(i / this.matrixSize) * paddingRatio;
        let newID = `shape${i}`;

        let newNode = shape.clone().attr("id", newID);
        //console.log(newNode);

        let position = `translate(${posX},${posY})`;
        // console.log(position);
        newNode.attr("transform", position);

        if ( i === this.targetIndex) {
          if (this.visualVariable === "Shape"){
            newNode.remove()
            targetShape.clone().attr("id", newID).attr("transform", position);
          }
          if (this.visualVariable === "Color"){
            // console.log("should remove " + shape.attr("id"));
            newNode.attr("fill", targetColor)
          }
        }
      }
    }
    shape.remove();
    targetShape.remove();
    this.timeCounter = Date.now();
    
  }

}
